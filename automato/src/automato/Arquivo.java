/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package automato;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

public class Arquivo {
        public ArrayList<String> conteudo = new ArrayList<>();
	public ArrayList<String> read(String caminho) throws UnsupportedEncodingException{
        try {
            File arquivo = new File(caminho);
            BufferedReader ler_arquivo = new BufferedReader(new InputStreamReader(new FileInputStream(arquivo),"UTF-8"));
            String linha="";
            try {
                linha = ler_arquivo.readLine();
                while(linha!=null){
                    this.conteudo.add(linha+"\n");
                    linha = ler_arquivo.readLine();
                }
                ler_arquivo.close();
                return this.conteudo;
            } catch (IOException ex) {
                return null;
            }
        } catch (FileNotFoundException ex) {
            return null;
        }
    }
}
