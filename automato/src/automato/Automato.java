/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package automato;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

/**
 *
 * @author willy
 */
public class Automato {
    public ArrayList<String> conteudo;
    public ArrayList<String> resposta = new ArrayList<>();
    public ArrayList<Integer> linhaEncontrada = new ArrayList<>();
    public Automato() throws UnsupportedEncodingException{
        this.BuscarArquivo();
    }
    public final void BuscarArquivo() throws UnsupportedEncodingException {
        String arq = "arq.txt";
        Arquivo arquivo = new Arquivo();
        this.conteudo = arquivo.read(arq);
        
    }   
    public void exibir(){
        for(String linha: conteudo){
            System.out.println(linha);
        }
    }
    
    public ArrayList<String> AcharCaracter(String subPalavra){
        ArrayList<String> palavras = new ArrayList<>();
        int contador = 1;
        for(String linha: conteudo){
            String palavra = "";
            boolean fimPalavra = false, salvar = false;
            String[] letrasLinha = linha.split("");
            for(int i = 0; i < letrasLinha.length; i++){
                switch(letrasLinha[i].toLowerCase()){
                        case "a": palavra += letrasLinha[i]; break;
                        case "à": palavra += letrasLinha[i]; break;
                        case "á": palavra += letrasLinha[i]; break;
                        case "â": palavra += letrasLinha[i]; break;
                        case "ã": palavra += letrasLinha[i]; break;
                        case "b": palavra += letrasLinha[i]; break;
                        case "c": palavra += letrasLinha[i]; break;
                        case "ç": palavra += letrasLinha[i]; break;
                        case "d": palavra += letrasLinha[i]; break;
                        case "e": palavra += letrasLinha[i]; break;
                        case "é": palavra += letrasLinha[i]; break;
                        case "ê": palavra += letrasLinha[i]; break;
                        case "f": palavra += letrasLinha[i]; break;
                        case "g": palavra += letrasLinha[i]; break;
                        case "h": palavra += letrasLinha[i]; break;
                        case "i": palavra += letrasLinha[i]; break;
                        case "í": palavra += letrasLinha[i]; break;
                        case "j": palavra += letrasLinha[i]; break;
                        case "k": palavra += letrasLinha[i]; break;
                        case "l": palavra += letrasLinha[i]; break;
                        case "m": palavra += letrasLinha[i]; break;
                        case "n": palavra += letrasLinha[i]; break;
                        case "o": palavra += letrasLinha[i]; break;
                        case "ó": palavra += letrasLinha[i]; break;
                        case "ô": palavra += letrasLinha[i]; break;
                        case "õ": palavra += letrasLinha[i]; break;
                        case "p": palavra += letrasLinha[i]; break;
                        case "q": palavra += letrasLinha[i]; break;
                        case "r": palavra += letrasLinha[i]; break;
                        case "s": palavra += letrasLinha[i]; break;
                        case "t": palavra += letrasLinha[i]; break;
                        case "u": palavra += letrasLinha[i]; break;
                        case "ú": palavra += letrasLinha[i]; break;
                        case "v": palavra += letrasLinha[i]; break;
                        case "w": palavra += letrasLinha[i]; break;
                        case "x": palavra += letrasLinha[i]; break;
                        case "y": palavra += letrasLinha[i]; break;
                        case "z": palavra += letrasLinha[i]; break;
                        case "-": palavra += letrasLinha[i]; break;
                        default: fimPalavra = true; break;     
                }  
                if(palavra.toLowerCase().equals(subPalavra.toLowerCase())){
                    salvar=true;
                }
                if(fimPalavra && salvar){
                    palavras.add(palavra);
                    this.linhaEncontrada.add(contador);
                    fimPalavra=false;
                    salvar=false;
                    palavra="";
                }else if(fimPalavra){
                    palavra="";
                    fimPalavra=false;
                }
            }
            contador += 1;
        }          
        return palavras;
    }
}
